package com.gmail.wieslawa.spring;

import com.gmail.wieslawa.spring.backend.entity.Conference;

import com.gmail.wieslawa.spring.views.design.LoginFormDesign;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;


@Route(value = MainView.MAIN_VIEW)
public class MainView extends VerticalLayout {


    public static final String MAIN_VIEW = "main";


    private Grid<Conference> grid = new Grid<>(Conference.class);


    public MainView() {

        grid.setItems();
        //grid.addColumn(Conference::);
        add(grid);

        Button button = new Button("Go to login page");
        button.addClickListener(event ->
                UI.getCurrent().navigate(LoginFormDesign.ROUTE));

        add(button);

    }

}
