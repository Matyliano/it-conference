package com.gmail.wieslawa.spring.backend.repository;


import com.gmail.wieslawa.spring.backend.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmail(String email);

    User save(User registration);


}
